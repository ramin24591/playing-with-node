import express from "express";
import bodyParser from "body-parser";
const server = express();

server.use(bodyParser.json());

import routes from "./routes";
routes(server);


export default server;
